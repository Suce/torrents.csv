import { Results, Torrent } from './interfaces';

export function magnetLink(infohash: string): string {
  return "magnet:?xt=urn:btih:" + infohash +
    "&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce" +
    "&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce" +
    "&tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce" +
    "&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80"
}

export function humanFileSize(bytes, si): string {
  var thresh = si ? 1000 : 1024;
  if (Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }
  var units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while (Math.abs(bytes) >= thresh && u < units.length - 1);
  return bytes.toFixed(1) + ' ' + units[u];
}

export function convertCsvToJson(csv: string): Results {

  let lines = csv.split("\n");
  let torrents: Array<Torrent> = [];

  for (let line of lines) {
    let cols = line.split(";");

    // Sometimes it gets back weird newlines
    if (cols[0]) {
      torrents.push({
        infohash: cols[0],
        name: cols[1],
        size_bytes: Number(cols[2]),
        created_unix: Number(cols[3]),
        seeders: Number(cols[4]),
        leechers: Number(cols[5]),
        completed: Number(cols[6]),
        scraped_date: Number(cols[7])
      });
    }
  }

  let result = {
    torrents: torrents
  }
  return result;
}