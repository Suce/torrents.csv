extern crate actix_web;
// extern crate Deserialize;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate pipers;

use actix_web::{fs, http, server, App, HttpResponse, Query};
use pipers::Pipe;

fn main() {
    server::new(|| {
        App::new()
            .route("/service/search", http::Method::GET, search)
            .handler(
                "/",
                fs::StaticFiles::new("../ui/dist/")
                    .unwrap()
                    .index_file("index.html"),
            )
            .finish()
    }).bind("127.0.0.1:8080")
        .unwrap()
        .run();
}

#[derive(Deserialize)]
struct SearchQuery {
    q: String,
    page: Option<u32>,
    size: Option<u32>,
}

fn search(query: Query<SearchQuery>) -> HttpResponse {
    HttpResponse::Ok()
        .header("Access-Control-Allow-Origin", "*")
        .content_type("text/csv")
        .body(ripgrep(query))
}

fn ripgrep(query: Query<SearchQuery>) -> String {
    let page = query.page.unwrap_or(1);
    let size = query.size.unwrap_or(10);
    let offset = size * (page - 1) + 1;
    let rg_search = format!("rg -i {} ../../torrents.csv", query.q.replace(" ", ".*"));

    println!(
        "search = {} , page = {}, size = {}, offset = {}",
        rg_search, page, size, offset
    );

    let out = Pipe::new(&rg_search)
        .then(format!("tail -n +{}", offset).as_mut_str())
        .then(format!("head -n {}", size).as_mut_str())
        .finally()
        .expect("Commands did not pipe")
        .wait_with_output()
        .expect("failed to wait on child");

    let mut results = format!("{}", String::from_utf8_lossy(&out.stdout));
    results.pop(); // Remove last newline for some reason
    results
}
