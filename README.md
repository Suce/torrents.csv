# Torrents.csv

`Torrents.csv` is a collaborative, *vetted* repository of torrents, consisting of a single, searchable `torrents.csv` file. Its initially populated with a January 2017 backup of the pirate bay, and new torrents are periodically added from various torrents sites via a rust script. 

`Torrents.csv` will only store torrents with at least one seeder to keep the file small, and will be periodically purged of non-seeded torrents, and sorted by seeders descending.

It also comes with a self-hostable [Torrents.csv webserver](https://torrents-csv.ml)

![img](https://i.imgur.com/qVmSVMC.png)

To request more torrents, or add your own to the file, submit a pull request here.

Made with [Rust](https://www.rust-lang.org), [ripgrep](https://github.com/BurntSushi/ripgrep), [Actix](https://actix.rs/), [Inferno](https://www.infernojs.org), [Typescript](https://www.typescriptlang.org/).

## Searching

### Requirements

- [ripgrep](https://github.com/BurntSushi/ripgrep)

### Running

To find torrents, run `./search.sh "bleh season 1"`
```
bleh season 1 (1993-)
	seeders: 33
	size: 13GiB
	link: magnet:?xt=urn:btih:INFO_HASH_HERE
```

## Running the webserver

### Requirements

- Rust
- ripgrep
- Yarn

### Running

`Torrents.csv` comes with a simple webserver. Run `cd scripts && ./webserver.sh`, and goto http://localhost:8080

## Uploading

An *upload*, consists of making a pull request after running the `add_torrents.sh` script, which adds torrents from a directory you choose to the `.csv` file, after checking that they aren't already there, and that they have seeders.

### Requirements

- [Torrent tracker scraper](https://github.com/ZigmundVonZaun/torrent-tracker-scraper)
- [Transmission-cli](https://transmissionbt.com/)
- [Human Friendly](https://humanfriendly.readthedocs.io/en/latest/readme.html#command-line)

### Running

[Click here](https://gitlab.com/dessalines/torrents.csv/forks/new) to fork this repo.
```sh
git clone https://gitlab.com/[MY_USER]/torrents.csv
cd torrents.csv/scripts
./add_torrents.sh MY_TORRENTS_DIR # `MY_TORRENTS_DIR` is `~/.local/share/data/qBittorrent/BT_backup/` for qBittorrent on linux, but you can search for where your torrents are stored for your client.
git commit -am "Adding my torrents"
git push
```

Then [click here](https://gitlab.com/dessalines/torrents.csv/merge_requests/new) to do a pull/merge request to my branch.

## How the file looks

```sh
infohash;name;size_bytes;created_unix;seeders;leechers;completed;scraped_date
# torrents here...
```

Made with [Rust](https://www.rust-lang.org), [ripgrep](https://github.com/BurntSushi/ripgrep), [Actix](https://actix.rs/), [Inferno](https://www.infernojs.org), and [Typescript](https://www.typescriptlang.org/).


## Potential sources for new torrents

- https://www.skytorrents.lol/top100
- https://1337x.to/top-100
- https://1337x.to/trending
