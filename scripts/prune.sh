# This prunes torrents.csv, removing those with too many columns, and sorts it
cd ..
torrents_csv="`pwd`/torrents.csv"

# Remove lines that don't have exactly 7 ';'
rg "^([^;]*;){7}[^;]+$" $torrents_csv > tmp_adds
mv tmp_adds $torrents_csv

# Remove random newlines
sed -i '/^$/d' $torrents_csv 

# Extract the header
header=$(head -n1 $torrents_csv) 
sed -i '1d' $torrents_csv

# Remove dups
sort -u -t';' -k1,1 -o $torrents_csv $torrents_csv

 # Sort by seeders desc
sort --field-separator=';' --key=5 -nr -o $torrents_csv $torrents_csv

# Add the header back in
sed  -i "1i $header" $torrents_csv
#truncate -s -1 $torrents_csv # Removing last newline


